# ChangeCounter
 
This application is a change counter, written in Python, where you give the coin counts and the application will calculate the total.

## Installation
To use:
1. download the zip file
1. extract the files
1. run the application (change_counter.exe) file

## How to Use
Begin by starting the application\
A window will pop up with entry boxes defaulted to zero for pennies, nickels, dimes, quarters, and half dollars.\
1. In the entry boxes, type in the appropriate total coin count next to the corresponding label.
1. When all entries are filled, click the *submit* button located at the bottom of the window and the program will calculate the coin price totals and display them directly to the right of the entry boxes. The overall total will be diplayed further to the right.
1. An alternative method is to use the weight (grams used for calculations:2 spots after the decimal is ideal). To use the weight option:
    1. click the *weight* button at the bottom of the window.
    1. A new window will open to the right of the primary window with entry boxes defaulted to zero for pennies, nickels, dimes, and quarters.
    1. When all entries are filled, click the *submit* button located at the bottom of the new window and the program will calculate the total coin counts for each coin (+ or - a few pennies). The program will then put those coin counts into their respective entry boxes in the primary window and the new window will close, returning to the primary window.
    1. Refer to *step 2* above.
    1. If you decide not to do the weight option, you can hit the *Quit* button located at the bottom of the new window and the new window will close, returning to the primary window.
1. When all done, hit the *Quit* button located at the bottom of the window to end and close the application.